package spec.ukr.putin.weapons;

import spec.ukr.putin.Voina;

public class Weapon {
    public String name;
    public float maxBreakChance;
    public int bulletsPerSecond;
    public int bulletDamage;

    public Weapon(String name, float maxBreakChance, int bulletsPerSecond, int bulletDamage) {
        this.name = name;
        this.maxBreakChance = maxBreakChance;
        this.bulletsPerSecond = bulletsPerSecond;
        this.bulletDamage = bulletDamage;
    }

    public int getDamage(float accuracy) {
        if (Voina.chance() <= Math.min(Voina.chance(), maxBreakChance)) {
            return 0;
        } else {
            return (int) Math.max((bulletsPerSecond * accuracy), 1) * bulletDamage;
        }
    }
}
