package spec.ukr.putin;

import spec.ukr.putin.people.Soldat;
import spec.ukr.putin.weapons.Weapon;

import java.util.ArrayList;
import java.util.Random;

public class Voina {
    public static Weapon ak47 = new Weapon("AK47", 0.1f, 5, 3);
    public static Weapon mp5 = new Weapon("MP5", 0.3f, 8, 2);

    public static ArrayList<Soldat> russianSoldats = new ArrayList<>();
    public static ArrayList<Soldat> ukraineSoldats = new ArrayList<>();

//    public static Soldat ruslan = new Soldat("Ruslan", 0.4f, 100, ak47);
//    public static Soldat taras = new Soldat("Taras", 0.7f, 100, mp5);

    public static void main(String[] args) {

        int rusScore = 0;
        int ukrScore = 0;

        for (int k = 0; k < 10000; k++) {

            for (int i = 1; i <= 20; i++) {
                russianSoldats.add(new Soldat("Русич "+i, 0.4f, 100, mp5));
            }

            for (int i = 1; i <= 20; i++) {
                ukraineSoldats.add(new Soldat("Тарас "+i, 0.4f, 100, mp5));
            }

            int totalRusHP;
            int totalUkrHP;
            do {
                simulate();

                totalRusHP = 0;
                totalUkrHP = 0;
                for (Soldat soldat: russianSoldats){
                    totalRusHP += soldat.HP;
                }
//                for (int ri = 0; ri < russianSoldats.size(); ri++) {
//                    totalRusHP += russianSoldats.get(ri).HP;
//                }

                for (Soldat soldat: ukraineSoldats) {
                    totalUkrHP += soldat.HP;
                }
//                for (int ui = 0; ui < ukraineSoldats.size(); ui++) {
//                    totalUkrHP += ukraineSoldats.get(ui).HP;
//                }
            } while (totalRusHP > 0 && totalUkrHP > 0);

            if (totalRusHP > 0) {
                rusScore++;
            } else if(totalUkrHP > 0) {
                ukrScore++;
            } else {
                rusScore++;
                ukrScore++;
            }

            russianSoldats.clear();
            ukraineSoldats.clear();
        }

        System.out.println("Хохлы / Русских = "+ukrScore+" / "+rusScore);
        System.out.println("Винрейт хозлов = "+(ukrScore/(float) rusScore));

    }

    public static void simulate(){
        crossFire(russianSoldats, ukraineSoldats);
        crossFire(ukraineSoldats, russianSoldats);

//        Oleg.HP = Oleg.HP + Tanya.healing();
//        if (Oleg.HP > 100) { Oleg.HP = 100; }
    }

    public static void crossFire(ArrayList<Soldat> whoIsShooting, ArrayList<Soldat> targets) {
        for (Soldat shooter: whoIsShooting){
            if (shooter.HP == 0) continue;
            int targetIndex = new Random().nextInt(0, targets.size());
            Soldat target = targets.get(targetIndex);

            shooter.fireAt(target);

            if (target.HP < 0) target.HP = 0;
        }
    }

    public static float chance() {
        return new Random().nextFloat(0, 1);
    }

//    public static void main(String[] args) {
//        System.out.println(AK47.bulletDamage);
//        System.out.println(AK47.bulletsPerSecond);
//        System.out.println(Petrus.accuracy);
//        int x = Petrus.fire();
//        System.out.println(x);

//        int a = 8; //Ox667 = 8
//        System.out.println("a before doStuff: "+a);
//        a = doStuff(a); //doStuff(Ox667);
//        System.out.println("a after doStuff: "+a); //0x667 выводим (8)
//    }

//    public static int doStuff(int x) { //0x985 = <- (8)
//        x++; //0x985 = 9 (8+1)
//        System.out.println("inside doStuff: "+x); //0x985 выводит (9)
//        return x;
//    } //Ox985 очищает
}
