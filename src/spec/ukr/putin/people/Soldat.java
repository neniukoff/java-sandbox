package spec.ukr.putin.people;

import spec.ukr.putin.weapons.Weapon;

public class Soldat {
    public String name;
    public float accuracy;
    public int HP;
    public Weapon weapon;

    public Soldat(String name, float accuracy, int HP, Weapon weapon) {
        this.name = name;
        this.accuracy = accuracy;
        this.HP = HP;
        this.weapon = weapon;
    }

    public void fireAt(Soldat target){
        target.HP -= weapon.getDamage(accuracy);
    }

    public void print(){
        System.out.println("У "+name+" "+HP+" ХП");
    }
}
