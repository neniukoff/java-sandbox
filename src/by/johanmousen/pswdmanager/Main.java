package by.johanmousen.pswdmanager;

import java.util.Scanner;

public class Main {
    private static final DataStorageUtils dataStorageUtils = new DataStorageUtils();
    private static final PasswordManagerActions passwordManagerActions = new PasswordManagerActions(dataStorageUtils);

    public static void main(String[] args) {
        while (true) {
            System.out.println("""
                Выберите действие:
                    1) Завести новую запись
                    2) Посмотреть записи
                    3) Удалить запись
                    4) Выход
                """);
            int action = new Scanner(System.in).nextInt();

            if (action == 1) {
                passwordManagerActions.createNewRecord();
            } else if (action == 2) {
                passwordManagerActions.showAllRecords();
            } else if (action == 3) {
                passwordManagerActions.deleteRecord();
            } else if (action == 4) {
                break;
            } else {
                System.out.println("Нет такеой команды!");
            }
        }
    }

}
