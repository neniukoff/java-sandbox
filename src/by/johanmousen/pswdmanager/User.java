package by.johanmousen.pswdmanager;

public class User {
    public String username;
    public String password;
    public String loginUrl;

    public User(String username, String password, String loginUrl) {
        this.username = username;
        this.password = password;
        this.loginUrl = loginUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public void print(){
        System.out.println(getPrintString());
    }

    public String getPrintString(){
        return "Username: "+username+", password: "+password;
    }
}
