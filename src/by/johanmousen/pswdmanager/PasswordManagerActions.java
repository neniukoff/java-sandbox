package by.johanmousen.pswdmanager;

import java.util.List;
import java.util.Scanner;

public class PasswordManagerActions {
    private final DataStorageUtils dataStorageUtils;

    public PasswordManagerActions(DataStorageUtils dataStorageUtils) {
        this.dataStorageUtils = dataStorageUtils;
    }

    public void createNewRecord(){
        System.out.println("Введите логин: ");
        String username = new Scanner(System.in).nextLine();

        System.out.println("Введите пароль: ");
        String pass = new Scanner(System.in).nextLine();

        System.out.println("Введите url: ");
        String url = new Scanner(System.in).nextLine();

        List<User> allUsers = dataStorageUtils.loadUsers();
        allUsers.add(new User(username, pass, url));
        dataStorageUtils.saveUsers(allUsers);
    }

    public void deleteRecord(){
        List<User> allUsers = dataStorageUtils.loadUsers();
        for (int i = 0; i < allUsers.size(); i++) {
            System.out.print(i+") ");
            System.out.println(allUsers.get(i).getPrintString());
        }
        System.out.println("Введите номер записи для удаления (-1 для отмены)");
        int choice = new Scanner(System.in).nextInt();
        if (choice != -1) {
            allUsers.remove(choice);
            dataStorageUtils.saveUsers(allUsers);
        }
    }

    public void showAllRecords(){
        List<User> allUsers = dataStorageUtils.loadUsers();
        for (User user: allUsers) { user.print(); }
    }

}
