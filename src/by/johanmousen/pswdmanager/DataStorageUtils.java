package by.johanmousen.pswdmanager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class DataStorageUtils {

    public List<User> loadUsers() {
        File file = new File("users");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String data = null;
        try {
            data = new String(Files.readAllBytes(Path.of("users")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (data == null || data.contentEquals("")) data = "[]";
        return new Gson().fromJson(data, new TypeToken<List<User>>(){}.getType());
    }

    public void saveUsers(List<User> userList) {
        File file = new File("users");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String data = new Gson().toJson(userList);
        try {
            Files.write(Path.of("users"), data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
