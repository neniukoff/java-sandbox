package com.johanmousen.shopsim.model;

public class GroceryProduct implements Product{
    private String name;
    private int buyRate;
    private int cost;

    public GroceryProduct(String name, int buyRate, int cost) {
        this.name = name;
        this.buyRate = buyRate;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getBuyRate() {
        return buyRate;
    }
    
    public void setBuyRate(int buyRate) {
        this.buyRate = buyRate;
    }
    
    public int getCost() {
        return cost;
    }
    
    public void setCost(int cost) {
        this.cost = cost;
    }
}
