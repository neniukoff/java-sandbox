package com.johanmousen.shopsim.model;

public class Bicycle implements Product {
    private String name;
    private int buyRate;
    private int cost;

    public Bicycle(String name, int buyRate, int cost) {
        this.name = name;
        this.buyRate = buyRate;
        this.cost = cost;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getBuyRate() {
        return buyRate;
    }

    @Override
    public void setBuyRate(int buyRate) {
        this.buyRate = buyRate;
    }

    @Override
    public int getCost() {
        return cost;
    }

    @Override
    public void setCost(int cost) {
        this.cost = cost;
    }
}
