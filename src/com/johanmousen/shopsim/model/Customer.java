package com.johanmousen.shopsim.model;

import com.johanmousen.shopsim.Cart;

import java.util.ArrayList;

public class Customer {
    private String name;
    private int additionalPurchaseChance;
    private int money;
    private Cart cart;

    public Customer(String name, int additionalPurchaseChance, int money) {
        this.name = name;
        this.additionalPurchaseChance = additionalPurchaseChance;
        this.money = money;
        this.cart = new Cart(new ArrayList<>());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAdditionalPurchaseChance() {
        return additionalPurchaseChance;
    }

    public void setAdditionalPurchaseChance(int additionalPurchaseChance) {
        this.additionalPurchaseChance = additionalPurchaseChance;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
}

