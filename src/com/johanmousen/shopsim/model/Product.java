package com.johanmousen.shopsim.model;

public interface Product {
    int getBuyRate();
    void setBuyRate(int buyRate);

    int getCost();
    void setCost(int cost);

    String getName();
    void setName(String name);
}
