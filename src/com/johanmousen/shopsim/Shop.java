package com.johanmousen.shopsim;

import com.johanmousen.shopsim.model.Bicycle;
import com.johanmousen.shopsim.model.Customer;
import com.johanmousen.shopsim.model.GroceryProduct;
import com.johanmousen.shopsim.model.Product;

import java.util.*;

public class Shop {

    public static void main(String[] args) {
        ArrayList<Customer> customerList = initializeCustomerList();
        ArrayList<Product> productList = initializeProductList();

        for (int l = 0; l < 20; l++) {
            for (Customer customer : customerList) {
                for (Product product : productList) {
                    if (!checkProductPurchaseChance(customer, product)) continue;
                    addProductToCustomerCart(product, customer);
                }
            }
        }

        for (Customer customer : customerList) {
            HashMap<String, Integer> productCountMap = new HashMap<>();
            System.out.println(customer.getName() + " " + customer.getMoney());

            for (Product product: customer.getCart().getProductList()){
                if (productCountMap.containsKey(product.getName())) {
                    productCountMap.put(product.getName(), productCountMap.get(product.getName()) + 1);
                } else {
                    productCountMap.put(product.getName(), 1);
                }
            }

            for (String key : productCountMap.keySet()) {
                System.out.println(key + ": " + productCountMap.get(key));
            }
        }
    }

    private static ArrayList<Product> initializeProductList() {
        ArrayList<Product> result = new ArrayList<>();
        result.add(new GroceryProduct("Сыр", 12, 15));
        result.add(new GroceryProduct("Мясо", 18, 20));
        result.add(new GroceryProduct("Хлеб", 6, 10));
        result.add(new Bicycle("Aist", 10, 40));
        return result;
    }

    private static ArrayList<Customer> initializeCustomerList() {
        ArrayList<Customer> result = new ArrayList<>();
        result.add(new Customer("Американец", 20, 1500));
        result.add(new Customer("Русский", 12, 1000));
        result.add(new Customer("Украинец", 15, 950));
        return result;
    }

    private static void addProductToCustomerCart(Product product, Customer customer) {
        customer.getCart().getProductList().add(product);
        customer.setMoney(customer.getMoney() - product.getCost());
        System.out.println(customer.getName() + " купил " + product.getName() + " у него осталось " + customer.getMoney());
    }

    private static boolean checkProductPurchaseChance(Customer customer, Product product) {
        return (product.getBuyRate() + customer.getAdditionalPurchaseChance()) > d100();
    }

    public static int d100 () {
        return new Random().nextInt(1, 101);
    }
}