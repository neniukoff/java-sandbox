package com.company;

public class Main {
    private static final float PI = (float) Math.PI;

    public static void main(String[] args) {
        printCylinder(12, 5);
        printCylinder(10, 2);
    }

    private static float circleArea(float r) {
        return PI * r * r;
    }

    private static float hui(float r){
        return PI * r * r;
    }

    private static float cylinderVolume(float area, float h) {
        return area * h;
    }

    public static void printCylinder(float r, float h) {
        // Для заданного радиуса образующего груга и высоты вывести объем цилиндра.

        float area = circleArea(r);
        float volume = cylinderVolume(area, h);

        System.out.println("Для цилиндра с радиусом основания "+r+" объем цилиндра будет равен "+volume+" при высоте равной "+h);
    }

}
