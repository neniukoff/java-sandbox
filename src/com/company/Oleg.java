package com.company;

public class Oleg {
    public static void main(String[] args) {
        // Седан на 1 км пути трати 0.78 литра топлива
        // Бмв на 1 км пути тратит 1.2 литра топлива
        // Объем бензобака Седана равняется 40 литров
        // Объем бензобака Бмв равняется 80 литров
        // Проехав 12 км, сколько бензина будет в бензобаке каждой машини?

        processCar("Седан", 12f, 0.78f, 40);
        processCar("БМВ", 12f, 1.2f, 80);
        processCar("Зэпор", 2f, 2.2f, 10);
    }

    public static float getSpentFuel(float fuelRate, float distance) {
        return fuelRate * distance;
    }

    public static float getLeftoverFuel(float spentFuel, float fuelVolume) {
        return fuelVolume - spentFuel;
    }

    public static void processCar(String carName, float distance, float fuelRate, float fuelVolume) {
        float spentFuel = getSpentFuel(fuelRate, distance);
        float leftoverFuel = getLeftoverFuel(spentFuel, fuelVolume);

        System.out.println("Машина "+carName+", проехав "+distance+" км, потратила "+spentFuel+" топлива. Осталось "+leftoverFuel+"/"+fuelVolume);
    }

}
