package com.gitlab.cvazer.showcase.abs;

import java.util.Map;
import java.util.regex.Pattern;

public interface Validatable {
    Map<String, Pattern> fieldsToValidate();
}
