package com.gitlab.cvazer.showcase.abs.processors;

public interface ObjectProcessor {
    void processObject(ProcessableObject obj);
}
