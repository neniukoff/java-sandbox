package com.gitlab.cvazer.showcase.abs.processors;

import com.google.gson.Gson;

public class PrintingObjectProcessor implements ObjectProcessor {
    @Override
    public void processObject(ProcessableObject obj) {
        System.out.println("All good");
        System.out.println(new Gson().toJson(obj));
    }
}
