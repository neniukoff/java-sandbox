package com.gitlab.cvazer.showcase.abs.processors;

import java.util.List;

public interface ProcessableObject {
    List<String> getSortedAddress();
}
