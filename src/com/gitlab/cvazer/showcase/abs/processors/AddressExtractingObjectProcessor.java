package com.gitlab.cvazer.showcase.abs.processors;

import com.google.gson.Gson;

import java.util.List;

public class AddressExtractingObjectProcessor implements ObjectProcessor {

    @Override
    public void processObject(ProcessableObject obj) {
        List<String> addrList = obj.getSortedAddress();
        Address address = new Address();
        for (int i = 0; i < addrList.size(); i++) {
            if (i == 0) address.setCountry(addrList.get(i));
            if (i == 1) address.setCity(addrList.get(i));
            if (i == 2) address.setStreet(addrList.get(i));
            if (i == 3) address.setHouse(addrList.get(i));
            if (i == 4) address.setApartment(addrList.get(i));
        }
        System.out.println(new Gson().toJson(address));
    }

    public static class Address {
        private String country;
        private String city;
        private String street;
        private String house;
        private String apartment;

        public Address() {}

        public Address(String country, String city, String street, String house, String apartment) {
            this.country = country;
            this.city = city;
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getHouse() {
            return house;
        }

        public void setHouse(String house) {
            this.house = house;
        }

        public String getApartment() {
            return apartment;
        }

        public void setApartment(String apartment) {
            this.apartment = apartment;
        }
    }
}
