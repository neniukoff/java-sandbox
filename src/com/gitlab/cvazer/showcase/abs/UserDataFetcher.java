package com.gitlab.cvazer.showcase.abs;

import com.gitlab.cvazer.showcase.model.UserData;

public class UserDataFetcher {
    public UserData getDataFromSomewhere() {
        return new UserData(
                "yan",
                "26",
                "Georgia, Batume, Lermontova, 105B, 47",
                "zhirnitoni@gmail.com"
        );
    }

}
