package com.gitlab.cvazer.showcase.abs.dispatchers;

import com.gitlab.cvazer.showcase.abs.processors.ObjectProcessor;
import com.gitlab.cvazer.showcase.abs.processors.AddressExtractingObjectProcessor;
import com.gitlab.cvazer.showcase.abs.processors.PrintingObjectProcessor;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class WeeklyUserDataDispatcher extends PlainUserDataDispatcher {
    @Override
    protected ObjectProcessor getCurrentProcessor() {
        if (LocalDate.now().getDayOfWeek() == DayOfWeek.FRIDAY) {
            return new AddressExtractingObjectProcessor();
        } else {
            return new PrintingObjectProcessor();
        }
    }
}
