package com.gitlab.cvazer.showcase.abs.dispatchers;

import com.gitlab.cvazer.showcase.abs.RegexValidator;
import com.gitlab.cvazer.showcase.model.UserData;
import com.gitlab.cvazer.showcase.abs.processors.ObjectProcessor;
import com.gitlab.cvazer.showcase.abs.UserDataFetcher;
import com.gitlab.cvazer.showcase.abs.processors.PrintingObjectProcessor;

public class PlainUserDataDispatcher extends AbstractUserDataDispatcher{
    protected @Override boolean validateObject(UserData data) { return new RegexValidator().validate(data); }
    protected @Override UserData getUserData() { return new UserDataFetcher().getDataFromSomewhere(); }
    protected @Override ObjectProcessor getCurrentProcessor() { return new PrintingObjectProcessor(); }
}
