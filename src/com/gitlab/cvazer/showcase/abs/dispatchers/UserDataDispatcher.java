package com.gitlab.cvazer.showcase.abs.dispatchers;

public interface UserDataDispatcher {
    void process();
}
