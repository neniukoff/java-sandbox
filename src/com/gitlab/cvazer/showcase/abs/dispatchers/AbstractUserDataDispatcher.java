package com.gitlab.cvazer.showcase.abs.dispatchers;

import com.gitlab.cvazer.showcase.abs.processors.ObjectProcessor;
import com.gitlab.cvazer.showcase.model.UserData;

public abstract class AbstractUserDataDispatcher implements UserDataDispatcher {
    @Override
    public void process() {
        ObjectProcessor currentProcessor = getCurrentProcessor();
        UserData data = getUserData();
        if (validateObject(data)){
            currentProcessor.processObject(data);
        }
    }

    protected abstract boolean validateObject(UserData data);
    protected abstract UserData getUserData();
    protected abstract ObjectProcessor getCurrentProcessor();

}
