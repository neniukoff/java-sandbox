package com.gitlab.cvazer.showcase.abs;

import java.util.Map;
import java.util.regex.Pattern;

public class RegexValidator {
    public boolean isStringFieldMatches(Pattern pattern, String value) {
        return pattern.matcher(value).matches();
    }

    public boolean validate(Validatable validatable) {
        Map<String, Pattern> data = validatable.fieldsToValidate();
        boolean valid = true;
        for (String key: data.keySet()) {
            if (!isStringFieldMatches(data.get(key), key)){
                valid = false;
                System.out.println("Error in value "+key+" against rule "+data.get(key).pattern());
                break;
            }
        }
        return valid;
    }
}
