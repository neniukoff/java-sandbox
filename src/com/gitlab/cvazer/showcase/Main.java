package com.gitlab.cvazer.showcase;

import com.gitlab.cvazer.showcase.abs.dispatchers.UserDataDispatcher;
import com.gitlab.cvazer.showcase.abs.dispatchers.WeeklyUserDataDispatcher;

public class Main {

    public static void main(String[] args) {
        UserDataDispatcher dataDispatcher = new WeeklyUserDataDispatcher();
        dataDispatcher.process();
    }

}
