package com.gitlab.cvazer.showcase.model;

import com.gitlab.cvazer.showcase.abs.Validatable;
import com.gitlab.cvazer.showcase.abs.processors.ProcessableObject;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class UserData implements Validatable, ProcessableObject {
    private String name; //только буквы a-z
    private String age; //только цифры
    private String address; //не более 5 наборов символов a-z0-9 разделенные запятой
    private String email; //только a-z и наличие знака @

    public UserData(String name, String age, String address, String email) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("Name changed from "+this.name+" to "+name);
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(int age) { setAge(""+age); }
    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Map<String, Pattern> fieldsToValidate() {
        return Map.ofEntries(
                Map.entry(name, Pattern.compile("^[a-z]*$")),
                Map.entry(age, Pattern.compile("^[0-9]*$")),
                Map.entry(address, Pattern.compile("^([a-zA-Z0-9]*(, )*)+$")),
                Map.entry(email, Pattern.compile("^[a-z0-9]+@[a-z]+[.][a-z]+$"))
        );
    }

    @Override
    public List<String> getSortedAddress() {
        return List.of(address.split(", "));
    }
}
