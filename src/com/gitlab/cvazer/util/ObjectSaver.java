package com.gitlab.cvazer.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

public class ObjectSaver {

    public void saveObject(Object obj){ saveObject(Collections.singleton(obj)); }
    public void saveObject(Collection<?> objects) {
        Map<Class<?>, List<Map<String, Object>>> data = getDataFromCollection(objects);
        checkFilesForClasses(data.keySet());
        data.forEach((clazz, list) -> {
            List<Map<String, Object>> savedData = getSavedData(clazz);
            if (savedData == null) savedData = new ArrayList<>();
            savedData.addAll(list);
            saveData(clazz, savedData);
        });
    }

    public <T> List<T> getObjects(Class<T> cls) {
        List<Map<String, Object>> data = getSavedData(cls);
        List<Constructor<?>> constructors = List.of(cls.getDeclaredConstructors());
//        data.stream()
//                .map(map -> {
//                    Object res = null;
//                    for (Constructor<?> constructor: constructors) {
//                        if (res != null) break;
//                        constructor.getParameterTypes().length
//                    }
//                })
        return null;
    }

    private void saveData(Class<?> clazz, List<Map<String, Object>> savedData) {
        List<String> lines = savedData.stream()
                .map(it -> it.entrySet().stream()
                        .map(e -> e.getKey() + ":" + e.getValue())
                        .collect(Collectors.joining(",")))
                .toList();
        try {
            Files.write(Paths.get(getStorageFileName(clazz)), lines, StandardOpenOption.WRITE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Map<String, Object>> getSavedData(Class<?> clazz) {
        try {
            return Files.readAllLines(Paths.get(getStorageFileName(clazz))).stream()
                    .filter(it -> !it.contentEquals(""))
                    .map(it -> {
                        List<String> fields = Arrays.asList(it.split(","));
                        Map<String, Object> map = new HashMap<>();
                        fields.forEach(field -> {
                            String[] data = field.split(":");
                            if (data.length < 2) return;
                            map.put(data[0], data[1]);
                        });
                        return map;
                    }).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void checkFilesForClasses(Set<Class<?>> classes) {
        classes.stream()
                .map(it -> new File(getStorageFileName(it)))
                .filter(it -> !it.exists())
                .forEach(it -> {
                    try {
                        //noinspection ResultOfMethodCallIgnored
                        it.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    private String getStorageFileName(Class<?> clazz) {
        return clazz.getSimpleName()+".txt";
    }

    private Map<Class<?>, List<Map<String, Object>>> getDataFromCollection(Collection<?> list) {
        Map<Class<?>, List<Map<String, Object>>> result = new HashMap<>();
        for (Object obj: list){
            if (obj instanceof Collection<?>) {
                concatDataSetsToFirst(result, getDataFromCollection((Collection<?>) obj));
            } else if(obj instanceof Map<?,?>) {
                throw new RuntimeException("Can't save maps!");
            } else {
                result.putIfAbsent(obj.getClass(), new ArrayList<>());
                result.get(obj.getClass()).add(pojoToDto(obj));
            }
        }
        return result;
    }

    private void concatDataSetsToFirst(
            Map<Class<?>, List<Map<String, Object>>> first,
            Map<Class<?>, List<Map<String, Object>>> second
    ) {
        second.forEach((clazz, list) -> {
            first.putIfAbsent(clazz, new ArrayList<>());
            first.get(clazz).addAll(list);
        });
    }

    private Map<String, Object> pojoToDto(Object pojo) {
        Map<String, Object> data = new HashMap<>();
        for (Field field: pojo.getClass().getDeclaredFields()) {
            boolean access = field.canAccess(pojo);
            field.setAccessible(true);
            Object value = null;
            try {
                value = field.get(pojo);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (value != null) data.put(field.getName(), value);
            field.setAccessible(access);
        }
        return data;
    }

}
