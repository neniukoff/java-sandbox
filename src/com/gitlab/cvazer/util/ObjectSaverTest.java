package com.gitlab.cvazer.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class ObjectSaverTest {
    public static void main(String[] args) {
        try {
            testCriticalPathSave();
        } catch (Exception e) {
            System.out.println("testCriticalPathSave failed: "+e.getMessage());
        }

        try {
            testSaveMap();
        } catch (Exception e) {
            System.out.println("testSaveMap failed: "+e.getMessage());
        }

        try {
            testSaveListOfA();
        } catch (Exception e) {
            System.out.println("testSaveListOfA failed: "+e.getMessage());
        }

        new ObjectSaver().getObjects(A.class);
    }

    private static void testSaveListOfA() {
        try {
            Files.deleteIfExists(Path.of("A.txt"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        List<A> list = List.of(
                new A(10, "20"),
                new A(1000, "2000")
        );
        new ObjectSaver().saveObject(list);

        if (!Files.exists(Path.of("A.txt"))) throw new RuntimeException("No file A.txt created");
        try {
            var lines = Files.readAllLines(Path.of("A.txt"));
            if (lines.size() != 2)
                throw new RuntimeException("Wrong lines number in A.txt should be 2");
            if (!lines.get(0).contentEquals("x:10,y:20"))
                throw new RuntimeException("The fist line should be x:10,y:20");
            if (!lines.get(1).contentEquals("x:1000,y:2000"))
                throw new RuntimeException("The second line should be x:1000,y:2000");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            Files.deleteIfExists(Path.of("A.txt"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void testSaveMap() {
        Map<String, Object> map = Map.of("test", "1");
        try {
            new ObjectSaver().saveObject(map);
            throw new RuntimeException("Map saved for some reason!");
        } catch (Exception ignored) {}
    }

    private static void testCriticalPathSave() {
        try {
            Files.deleteIfExists(Path.of("A.txt"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        A a = new A(10, "20");
        new ObjectSaver().saveObject(a);
        if (!Files.exists(Path.of("A.txt"))) throw new RuntimeException("No file A.txt created");
        try {
            var lines = Files.readAllLines(Path.of("A.txt"));
            if (lines.size() != 1)
                throw new RuntimeException("Wrong lines number in A.txt should be 1");
            if (!lines.get(0).contentEquals("x:10,y:20"))
                throw new RuntimeException("The line should be x:10,y:20");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            Files.deleteIfExists(Path.of("A.txt"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static class A {
        private int x;
        private String y;



        public A(int x, String y) {
            this.x = x;
            this.y = y;
        }
    }
}
