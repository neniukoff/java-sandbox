package com.gitlab.cvazer.inheritance;

import lombok.AllArgsConstructor;

import java.util.Objects;


@AllArgsConstructor
public class A {
    private int x;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        A a = (A) o;
        return x == a.x;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x);
    }
}
