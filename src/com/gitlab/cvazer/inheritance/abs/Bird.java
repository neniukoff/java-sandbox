package com.gitlab.cvazer.inheritance.abs;

import com.gitlab.cvazer.inheritance.abs.composition.SingingSpeaker;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class Bird extends Animal {
    private double wingSpan;

    public Bird(double weight, int height, double wingSpan) {
        super(new SingingSpeaker(), weight, height);
        this.wingSpan = wingSpan;
    }

}
