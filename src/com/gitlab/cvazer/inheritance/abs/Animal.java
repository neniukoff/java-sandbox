package com.gitlab.cvazer.inheritance.abs;

import com.gitlab.cvazer.inheritance.abs.composition.Speaker;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class Animal {
    private Speaker speaker;
    private double weight;
    private int height;
}
