package com.gitlab.cvazer.inheritance.abs;

import com.gitlab.cvazer.inheritance.abs.composition.BarkingSpeaker;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class Fox extends Animal {
    private String color;

    public Fox(double weight, int height, String color) {
        super(BarkingSpeaker.getInstance(), weight, height);
        this.color = color;
    }
}
