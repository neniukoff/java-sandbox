package com.gitlab.cvazer.inheritance.abs.composition;

public interface Speaker {
    void noise();
}
