package com.gitlab.cvazer.inheritance.abs.composition;

public class BarkingSpeaker implements Speaker {
    private static BarkingSpeaker instance;

    private BarkingSpeaker() {}

    public static BarkingSpeaker getInstance(){
        if (instance == null) {
            instance = new BarkingSpeaker();
        }
        return instance;
    }

    @Override
    public void noise() {
        System.out.println("Gav-gav");
    }
}
