package com.gitlab.cvazer.inheritance.abs.composition;

public class SingingSpeaker implements Speaker{
    @Override
    public void noise() {
        System.out.println("Chik-chirik");
    }
}
