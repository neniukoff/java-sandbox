package com.gitlab.cvazer.inheritance.abs.composition;

public class PudelSpeaker implements Speaker{
    @Override
    public void noise() {
        System.out.println("Wof-wof");
    }
}
