package com.gitlab.cvazer.inheritance.abs;

import com.gitlab.cvazer.inheritance.abs.composition.BarkingSpeaker;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class Dog extends Animal {
    private String nick;

    public Dog(double weight, int height, String nick) {
        super(BarkingSpeaker.getInstance(), weight, height);
        this.nick = nick;
    }
}
