package com.gitlab.cvazer.inheritance.birds;

import com.gitlab.cvazer.inheritance.abs.Bird;
import com.gitlab.cvazer.inheritance.abs.composition.VoronaSpeaker;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Vorona extends Bird {
    public Vorona(double weight, int height, double wingSpan) {
        super(weight, height, wingSpan);
        setSpeaker(new VoronaSpeaker());
    }
}
