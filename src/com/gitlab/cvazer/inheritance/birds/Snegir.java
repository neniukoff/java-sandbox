package com.gitlab.cvazer.inheritance.birds;

import com.gitlab.cvazer.inheritance.abs.Bird;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Snegir extends Bird {
    public Snegir(double weight, int height, double wingSpan) {
        super(weight, height, wingSpan);
    }
}
