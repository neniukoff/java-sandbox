package com.gitlab.cvazer.inheritance.dogs;

import com.gitlab.cvazer.inheritance.abs.Dog;
import com.gitlab.cvazer.inheritance.abs.composition.PudelSpeaker;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Pudel extends Dog {
    public Pudel(double weight, int height, String nick) {
        super(weight, height, nick);
        setSpeaker(new PudelSpeaker());
    }
}
