package com.gitlab.cvazer.inheritance.dogs;

import com.gitlab.cvazer.inheritance.abs.Dog;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Haski extends Dog {
    public Haski(double weight, int height, String nick) {
        super(weight, height, nick);
    }
}
