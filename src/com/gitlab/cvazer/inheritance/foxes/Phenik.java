package com.gitlab.cvazer.inheritance.foxes;

import com.gitlab.cvazer.inheritance.abs.Fox;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Phenik extends Fox {
    public Phenik(double weight, int height, String color) {
        super(weight, height, color);
    }
}
