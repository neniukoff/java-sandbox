package com.gitlab.cvazer.inheritance;

import com.gitlab.cvazer.inheritance.abs.Animal;
import com.gitlab.cvazer.inheritance.birds.Snegir;
import com.gitlab.cvazer.inheritance.birds.Vorona;
import com.gitlab.cvazer.inheritance.dogs.Chihuahua;
import com.gitlab.cvazer.inheritance.dogs.Haski;
import com.gitlab.cvazer.inheritance.dogs.Pudel;
import com.gitlab.cvazer.inheritance.foxes.Phenik;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Animal> list = new ArrayList<>();
        list.add(new Snegir(0.6, 8, 20));
        list.add(new Vorona(0.8, 9, 25));
        list.add(new Haski(30, 40, "Jopa"));
        list.add(new Pudel(15, 20, "Grisha"));
        list.add(new Chihuahua(15, 30, "Sharik"));
        list.add(new Phenik(2, 8, "Red"));

        for (Animal obj: list) {
            System.out.print(obj.getClass().getSimpleName()+": ");obj.getSpeaker().noise();
        }
    }
}
